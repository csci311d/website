<?php
	//include db connection stuff
	require_once("dbinfo.inc");
	
	function password_encrypt($password){
		$hash_format = "$2y$10$";
		$salt_length = 22;
		$salt = generate_salt($salt_length);
		$format_and_salt = $hash_format.$salt;
		$hash = crypt($password, $format_and_salt);
		return $hash;
	}	
	
	function generate_salt($length){
		//generate pseudo random string (good enough)
		//returns 32 characters
		$unique_random_string = md5(uniqid(mt_rand(), true));
		
		//convert it to base 64 (valid chars are [a-zA-Z0-0./] )
		$base64_string = base64_encode($unique_random_string);
		
		//remove the '+' characters, just replace with '.'
		$modified_base64_string = str_replace('+', '.', $base64_string);
		
		//truncate off just what we need
		$salt = substr($modified_base64_string, 0, $length);
		
		return $salt;
	}
	function password_check($password, $existing_hash){
		$hash = crypt($password, $existing_hash);
		if($hash === $existing_hash){
			return true;
		}else{
			return false;
		}
	}
	function createAccount($first_name, $last_name,  $password, $email, $phone, $grade){
		//echo "grade ". $grade;
		global $dbservername, $dbdatabase, $dbusername, $dbpassword;			
		$myHandle;
		try{
			$myHandle = new PDO("mysql:host=$dbservername;dbname=$dbdatabase", $dbusername, $dbpassword);

		}catch(PDOException $e){
			$err .= "Connection failed: " . $e->getMessage() . "\n";
			
		}		
		if($myHandle){
			//check that the user exists
			$myStmt = $myHandle->prepare("SELECT count(*) as total FROM Signup WHERE email=:mail");
			$myStmt->bindParam(':mail', $email);
			$myStmt->execute();
			
			$count = $myStmt->fetchColumn();
			if($count == 0){
				//username does not exist yet, so let's create it
				$sql = "INSERT into Signup(firstname, lastname, password, email, grade, phone) VALUES(:u_fname, :u_lname, :p_word, :mail, :grade, :phone)";
				$insertStmt = $myHandle->prepare($sql);
				//hash the password
				$hashed_pw = password_encrypt($password);
				$insertStmt->bindParam(':u_fname', $first_name);
                $insertStmt->bindParam(':u_lname', $last_name);
				$insertStmt->bindParam(':p_word', $hashed_pw);
				$insertStmt->bindParam(':mail', $email);
				$insertStmt->bindParam(':grade', $grade);
                $insertStmt->bindParam(':phone', $phone);
				
				$insertStmt->execute();
				return true;
			}else{
				return false;			
			}
		}
		$myHandle = null;
		return false;
	}
	 function attempt_login($email, $passwordAttempt){
		//check that the user exists
		//connect to db, 
		global $dbservername, $dbdatabase, $dbusername, $dbpassword;
		$myHandle;
		try{
			$myHandle = new PDO("mysql:host=$dbservername;dbname=$dbdatabase", $dbusername, $dbpassword);

		}catch(PDOException $e){
			$err .= "Connection failed: " . $e->getMessage() . "\n";
		}	
		
		if($myHandle){
			//make sure user exists
			$hashed_pw;
			$fname;
			$myStmt = $myHandle->prepare("SELECT  FirstName, email, password, grade FROM Signup WHERE email=:mail");
			$myStmt->bindParam(':mail', $email);
			$myStmt->execute();
			$rslt = $myStmt->fetchAll();
			if(count($rslt) > 0){
				//good
				//we have the hashed password for this user
				//check it against the one they entered
				foreach($rslt as $row){
					//print_r($row);
					$fname = $row['FirstName'];
					$hashed_pw = $row['password'];
					$grade = $row['grade'];
				}
				
				//then check it
				if(password_check($passwordAttempt, $hashed_pw)){
					$_SESSION['UserData']['Firstname'] = $fname;
					$_SESSION['UserData']['Grade'] = $grade;
					//echo "Grade: ". $_SESSION['UserData']['Grade'];
					return true;
				}else{
					return false;
					
				}
			}else{
				return false;
			}
		}
		//something else bad happened
		return false;
		
	} 
	
	
?>
