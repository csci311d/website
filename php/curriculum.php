<?php
session_start(); /* Starts the session */
$status;
require_once("../Private/inputHelper.php");
require_once("../Private/loginHelpers.php");

if(isset($_SESSION['UserData']['Firstname'])){
	$status=2;
}
if(isset($_POST['submit'])){
	$form_name=$_POST['first_name'];
	$form_psw=$_POST['psw'];

	if(attempt_login($form_name, $form_psw) ){
		$_SESSION['UserData']['Firstname']=$form_name;
		$status = 1;
	}else{
		$status = 0;
	}
}
?>
<!DOCTYPE html>
<html lang="en">

  <head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Curriculum</title>

    <!-- Bootstrap core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom fonts for this template -->
   

    <!-- Custom styles for this template -->
    <link href="../css/business-casual.min.css" rel="stylesheet">

  </head>

  <body>

    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
     <!-- <span class="site-heading-lower">Business Casual</span> -->
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">RK's Maths Tutorial</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-expanded" href="index.php">Home
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item px-lg-4 dropdown">
        
                <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >About</a>
            
                <ul class="dropdown-menu">
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                    </li>
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                    </li>
                </ul>
            </li>
 
            <li class="nav-item active px-lg-4 dropdown">
        
                <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >Curriculum</a>
            
                <ul class="dropdown-menu">
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                    </li>
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                    </li>
                    <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                    </li>
                   <li class="nav-item px-lg-4">
                        <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                    </li>
                </ul>
            </li>
                      
          </ul>
            
   
                    <ul class="navbar-nav mx-auto ">
            <li class="nav-item px-lg-4"><i  class="nav-link  text-expanded" >Hello, <?php echo $_SESSION['UserData']['Firstname'];?></i><span class="glyphicon glyphicon-log-in"></span></li>
            <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href=" logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
        </ul>
            
        </div>
      </div>
    </nav>
      
      <footer class="footer text-faded text-center py-5">
      <div class="container">
        <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
      </div>
    </footer>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  </body>

</html>
