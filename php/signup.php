
<?php
/*
	<!--

    Refernce for email regex: https://www.regular-expressions.info/email.html

	-->
*/

session_start(); /* Starts the session */
require_once ("../Private/loginHelpers.php");
require_once ("../Private/inputHelper.php");
$form_first_name;
$form_last_name;
$form_password;
$form_confirm;
$form_email;
$form_phone;
$form_grade;
$valid = true;
$status;
$err = "";
if (isset($_POST['submit'])) {
    $form_first_name = $_POST['first_name'];
    $form_last_name = $_POST['last_name'];
    $form_password = $_POST['psw'];
    $form_confirm = $_POST['cpsw'];
    $form_email = $_POST['email'];
    $form_phone = $_POST['phone'];
    $form_grade = $_POST['grade'];
    if (!has_presence($form_first_name) || !has_presence($form_last_name) || !has_presence($form_password) || !has_presence($form_confirm) || !has_presence($form_email) || !has_presence($form_phone) || !has_presence($form_grade)) {
        //bad things happenned
        $valid = false;
    }
    if (!has_length($form_first_name, ['min' => 2, 'max' => 20]) || !has_length($form_last_name, ['min' => 2, 'max' => 20]) || !has_length($form_password, ['min' => 6, 'max' => 20]) ||  !has_length($form_confirm, ['min' => 6, 'max' => 20]) || !has_length($form_email, ['min' => 6, 'max' => 40]) || !has_length($form_phone, ['exact' => 10]) || !has_length($form_grade, ['min' => 1, 'max' => 2])) {
        //bad things happenned
        $valid = false;
    }

    $re = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/';

    $rp = '/^[1-9]{1}[0-9]{9}$/';
    
    $rg = '/^[9]|[10]|[11]|[12]$/';

    if (! has_format_matching($form_email, $re )){
        $valid = false;
			$err = $err."Email entered is not valid! \\n";

    }

    if (! has_format_matching($form_phone, $rp )){
        $valid = false;
			$err = $err."Phone entered is not valid! \\n";

    } 

    if (! has_format_matching($form_grade, $rg )){
        $valid = false;
			$err = $err."Grade entered is not valid! \\n";

    }   
    
    //validate and sanitize input
    if ($valid) {
        $form_first_name = htmlspecialchars($form_first_name);
        $form_last_name = htmlspecialchars($form_last_name);
        $form_password = htmlspecialchars($form_password);
        $form_confirm = htmlspecialchars($form_confirm);
        $form_email = htmlspecialchars($form_email);
        $form_phone = htmlspecialchars($form_phone);
        $form_grade = htmlspecialchars($form_grade);
    }
    
    if ($form_confirm === $form_password){

        //create account
	    if ($valid && createAccount($form_first_name, $form_last_name, $form_password, $form_email, $form_phone, $form_grade)) {
            $status = 1;
            //go to home page
            //set post variable
            $_SESSION['UserData']['Firstname'] = $form_first_name;
            $_SESSION['UserData']['Grade'] = $form_grade;
            $_SESSION['UserData']['Email'] = $form_email;
            if ($_SESSION['UserData']['Grade'] == '12') {
                header("location:12.php");
            }
		    elseif ($_SESSION['UserData']['Grade'] == '11') {
                header("location:11.php");
            }
            elseif ($_SESSION['UserData']['Grade'] == '10') {
                header("location:10.php");
            }
            elseif ($_SESSION['UserData']['Grade'] == '9') {
                header("location:9.php");
            } 
	        else {
                header("location: index.php");
            }
            exit;
        } 
        else {
            $status = 0;
				$err = $err."Failed to create account. \\nPlease try again.";
            echo "<script>alert('$err');</script>";
        }

    }
    else{
			$err = $err."password did not match with confirm password \\nPlease try again.";
        echo "<script>alert('$err');</script>";
    }

}
?>


<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Signup: RK's Maths Tutorial</title>
      <!-- Bootstrap core CSS -->
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="../css/business-casual.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../css/login.css" >
   </head>
   <body id = "main">
      <h1 class="site-heading text-center text-white d-none d-lg-block">
         <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
         <!-- <span class="site-heading-lower">Business Casual</span> -->
      </h1>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
         <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">RK's Maths Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item  px-lg-4">
                     <a class="nav-link text-uppercase text-expanded" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="instructor.php" data-toggle="dropdown" >About</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="curriculum.php" data-toggle="dropdown" >Curriculum</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                        </li>
                     </ul>
                  </li>
               </ul>
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4 active"><a class="nav-link text-uppercase text-expanded" style="color: silver;" id = "c" href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
               </ul>
            </div>
         </div>
      </nav>
      <form class="login-content" method="post">
         <div class="container">
            <h1>Sign Up</h1>
            <p>Please fill the form to create an account!</p>

            <label for="name"><b>First Name*</b></label>
            <input type="text" placeholder="Enter First Name" name="first_name" id="name"  autofocus title="Minimum 2 letters required" required>
            <label for="lastname"><b>Last Name*</b></label>
            <input type="text" placeholder="Enter Last Name" name="last_name"  id="lastname" title="Minimum 2 letters required" required>
            <br><label for="email"><b>Email*</b></label><br>
            <input type="text" placeholder="Enter Email" name="email" id="email" title= "email" required>
            <br><label for="phone"><b>Phone number*</b></label>
            <input type="text" placeholder="Enter Phone number" name="phone"  id="phone"   title="Should be a 10 digit number" required>
            <br><label for="grade"><b>Select your Grade*</b></label> &nbsp; &nbsp; &nbsp;

            <select id="grade" name="grade">
               <option value="9">9</option>
               <option value="10">10</option>
               <option value="11">11</option>
               <option value="12">12</option>
            </select>
            <br>      
            <br><label for="psw"><b>Password*</b></label>
            <input type="password" placeholder="Enter Password" name="psw" id="psw" title="Must contain mimimum of 6 or more characters" required>
            <br><label for="cpsw"><b>Confirm Password*</b></label>
            <input type="password" placeholder="Confirm Password" name="cpsw"  id="cpsw"  title="Must contain mimimum of 6 or more characters" required>
            <div class="clearfix">
               <button type="submit" class="loginbtn" name="submit">Sign Up</button>
            </div>
         </div>
      </form>
      <footer class="footer text-faded text-center py-5">
         <div class="container">
            <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="../vendor/jquery/jquery.min.js"></script>
      <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   </body>
</html>


