<!--
Created by Explorers
Last Modified: 7th April

-->

<?php
   session_start(); /* Starts the session */
   if(!isset($_SESSION['UserData']['Email'])){
   	$login = 0;//user not logged in
   }
   else{
   	$login = 1;//user logged in
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>10th class curriculum</title>
      <!-- Bootstrap core CSS -->
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="../css/business-casual.min.css" rel="stylesheet">
      <link href="../css/9.css" rel="stylesheet">
   </head>
   <body>
      <h1 class="site-heading text-center text-white d-none d-lg-block">
         <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
         <!-- <span class="site-heading-lower">Business Casual</span> -->
      </h1>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
         <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">RK's Maths Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item px-lg-4">
                     <a class="nav-link text-uppercase text-expanded" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >About</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item px-lg-4 dropdown active">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" style="color: silver;" >Curriculum</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                        </li>
                        <li class="nav-item px-lg-4 active">
                           <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                        </li>
                     </ul>
                  </li>
               </ul>
               <?php
                  if($login === 1){
                  ?>
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4">
                     <i  class="nav-link  text-expanded" >
                     <?php 
                        echo "Hello, ";					
                        echo $_SESSION['UserData']['Firstname'];
                        ?>
                     </i><span class="glyphicon glyphicon-log-in"></span>
                  </li>
                  <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href=" logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
               </ul>
               <?php
                  }elseif($login === 0){
                  ?>
              
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
               </ul>
               <?php
                  }
                  ?>    
            </div>
         </div>
      </nav>
	<br>
      		<h2 style="color:white; text-align:center; " >10<sup>th </sup>Grade Curriculum</h2>
      <?php
         if( ($login === 1) && ($_SESSION['UserData']['Grade'] == '10') ){
         ?>
      <section class="page-section cta">

         <div class="container">
            <div class="row">
               <div class="col-xl-12 mx-auto outer">
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse1" id="symbolObjective">Objective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item">Chapter 2 - Polynomials</li>
                              <li class="list-group-item">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item">Chapter 4 - Quadratic Equations</li>
                              <li class="list-group-item">Chapter 5 - Arithmetic Progressions</li>
                              <li class="list-group-item">Chapter 6 - Triangles</li>
                              <li class="list-group-item">Chapter 7 - Coordinate Geometry</li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1200" target="_blank">Chapter 8 - Introduction to Trigonometry</a></li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1201" target="_blank">Chapter 9 - Some Applications of Trigonometry</a></li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1202" target="_blank">Chapter 10 - Circles</a></li>
                              <li class="list-group-item">Chapter 11 - Constructions</li>
                              <li class="list-group-item">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item">Chapter 13 - Surface Areas and Volumes</li>
                              <li class="list-group-item">Chapter 14 - Statistics</li>
                              <li class="list-group-item">Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse2" id="symbolSubjective">Subjective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item">Chapter 2 - Polynomials</li>
                              <li class="list-group-item">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1196" target="_blank">Chapter 4 - Quadratic Equations</a></li>
                              <li class="list-group-item">Chapter 5 - Arithmetic Progressions</li>
                              <li class="list-group-item">Chapter 6 - Triangles</li>
                              <li class="list-group-item">Chapter 7 - Coordinate Geometry</li>
                              <li class="list-group-item">Chapter 8 - Introduction to Trigonometry</li>
                              <li class="list-group-item">Chapter 9 - Some Applications of Trigonometry</li>
                              <li class="list-group-item">Chapter 10 - Circles</li>
                              <li class="list-group-item">Chapter 11 - Constructions</li>
                              <li class="list-group-item">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1205" target="_blank">Chapter 13 - Surface Areas and Volumes</a></li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1206" target="_blank">Chapter 14 - Statistics</a></li>
                              <li class="list-group-item">Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse3" id="symbolTest">Test Series &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item">Chapter 2 - Polynomials</li>
                              <li class="list-group-item">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item">Chapter 4 - Quadratic Equations</li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1197" target="_blank">Chapter 5 - Arithmetic Progressions</a></li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1198" target="_blank">Chapter 6 - Triangles</a></li>
                              <li class="list-group-item"><a href="https://schools.aglasem.com/1199" target="_blank">Chapter 7 - Coordinate Geometry</a></li>
                              <li class="list-group-item">Chapter 8 - Introduction to Trigonometry</li>
                              <li class="list-group-item">Chapter 9 - Some Applications of Trigonometry</li>
                              <li class="list-group-item">Chapter 10 - Circles</li>
                              <li class="list-group-item">Chapter 11 - Constructions</li>
                              <li class="list-group-item">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item">Chapter 13 - Surface Areas and Volumes</li>
                              <li class="list-group-item">Chapter 14 - Statistics</li>
                              <li class="list-group-item">Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php
         }else{
         
         ?>
      <section class="page-section cta">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 mx-auto outer">
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse1" id="symbolObjective" title="Click to view content.">Objective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Polynomials</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Quadratic Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Arithmetic Progressions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Triangles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content."> Chapter 7 - Coordinate Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Introduction to Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Some Applications of Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Constructions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Surface Areas and Volumes</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 14 - Statistics</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content."> Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse2" id="symbolSubjective" title="Click to view content.">Subjective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Polynomials</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Quadratic Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Arithmetic Progressions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Triangles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 7 - Coordinate Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Introduction to Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Some Applications of Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Constructions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Surface Areas and Volumes</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 14 - Statistics</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse3" id="symbolTest" title="Click to view content.">Test Series &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Real Numbers</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Polynomials</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Pair of Linear Equations in Two Variables</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Quadratic Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Arithmetic Progressions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Triangles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 7 - Coordinate Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Introduction to Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Some Applications of Trigonometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Constructions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Areas Related to Circles</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Surface Areas and Volumes</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 14 - Statistics</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 15 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php
         }
         ?> 
      <footer class="footer text-faded text-center py-5">
         <div class="container">
            <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="../vendor/jquery/jquery.min.js"></script>
      <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- customised js for this curriculum pages -->
      <script src="../js/curriculum.js"></script>
			<?php 
			if( $login === 0 ){
		?>
		<script src="../js/alrt.js"></script> 
		<?php
			}
		?>
   </body>
</html>
