<!--
Created by Explorers
Last Modified: 7th April

-->


<?php
   session_start(); /* Starts the session */
   if(!isset($_SESSION['UserData']['Email'])){
   	$login = 0;//user not logged in
   }
   else{
   	$login = 1;//user logged in
   }
   ?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>12th class curriculum</title>
      <!-- Bootstrap core CSS -->
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom styles for this template -->
      <link href="../css/business-casual.min.css" rel="stylesheet">
      <link href="../css/9.css" rel="stylesheet">
   </head>
   <body>
      <h1 class="site-heading text-center text-white d-none d-lg-block">
         <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
         <!-- <span class="site-heading-lower">Business Casual</span> -->
      </h1>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
         <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#" >RK's Maths Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item px-lg-4">
                     <a class="nav-link text-uppercase text-expanded" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >About</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item px-lg-4 dropdown active">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle " href="#" data-toggle="dropdown" style="color: silver;">Curriculum</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                        </li>
                        <li class="nav-item px-lg-4 active">
                           <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                        </li>
                     </ul>
                  </li>
               </ul>
               <?php
                  if($login === 1){
                  ?>
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4">
                     <i  class="nav-link  text-expanded" >
                     <?php 
                        echo "Hello, ";					
                        echo $_SESSION['UserData']['Firstname'];
                        ?>
                     </i><span class="glyphicon glyphicon-log-in"></span>
                  </li>
                  <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href=" logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
               </ul>
               <?php
                  }elseif($login === 0){
                  ?>
             
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
               </ul>
               <?php
                  }
                  ?> 
            </div>
         </div>
      </nav><br>
      <h2 style="color:white; text-align:center; ">12<sup>th </sup>Grade Curriculum</h2>
      <?php
         if( ($login === 1) && ($_SESSION['UserData']['Grade'] == '12') ){
         ?>
      <section class="page-section cta">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 mx-auto outer">
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse1" id="symbolObjective">Objective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <!--Objective type -->
                        <div id="collapse1" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item"><a href="../data/objective/Relations%20and%20Functions.pdf" target="_blank" title="Click to Open">Chapter 1 - Relations and Functions</a></li>
                              <li class="list-group-item"><a href="../data/objective/Inverse%20Trigonometric%20Functions.pdf" target="_blank" title="Click to Open">Chapter 2 - Inverse Trigonometric Functions</a></li>
                              <li class="list-group-item"><a href="../data/objective/Matrices.pdf" target="_blank" title="Click to Open">Chapter 3 - Matrices</a></li>
                              <li class="list-group-item"><a href="../data/objective/Determinants.pdf" target="_blank" title="Click to Open">Chapter 4 - Determinants</a></li>
                              <li class="list-group-item"><a href="../data/objective/Continuity%20and%20Differentiability.pdf" target="_blank" title="Click to Open">Chapter 5 - Continuity and Differentiability</a></li>
                              <li class="list-group-item"><a href="../data/objective/Application%20of%20Derivatives.pdf" target="_blank" title="Click to Open">Chapter 6 - Application of Derivatives</a></li>
                              <li class="list-group-item"><a href="../data/objective/Integrals.pdf" target="_blank" title="Click to Open">Chapter 7 - Integrals</a></li>
                              <li class="list-group-item"><a href="../data/objective/Application%20of%20Integrals.pdf" target="_blank" title="Click to Open">Chapter 8 - Application of Integrals</a></li>
                              <li class="list-group-item">Chapter 9 - Differential Equations</li>
                              <li class="list-group-item">Chapter 10 - Vector Algebra</li>
                              <li class="list-group-item"><a href="../data/objective/Three%20Dimensional%20Geometry.pdf" target="_blank" title="Click to Open">Chapter 11 - Three Dimensional Geometry</a></li>
                              <li class="list-group-item">Chapter 12 - Linear Programming</li>
                              <li class="list-group-item"><a href="../data/objective/Probability.pdf" target="_blank" title="Click to Open">Chapter 13 - Probability</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse2" id="symbolSubjective">Subjective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <!--Subjective type -->
                        <div id="collapse2" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item"><a href="../data/subjective/Relations%20and%20Functions.pdf" target="_blank" title="Click to Open"> Chapter 1 - Relations and Functions</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Inverse%20Trigonometric%20Functions.pdf" target="_blank" title="Click to Open">Chapter 2 - Inverse Trigonometric Functions</a></li>
                              <li class="list-group-item"><a href="../data/subjective/matrices.pdf" target="_blank" title="Click to Open">Chapter 3 - Matrices</a></li>
                              <li class="list-group-item"><a href="../data/subjective/determ.pdf" target="_blank" title="Click to Open">Chapter 4 - Determinants</a></li>
                              <li class="list-group-item"><a href="../data/subjective/cont.pdf" target="_blank" title="Click to Open">Chapter 5 - Continuity and Differentiability</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Applications%20of%20Derivatives.pdf" target="_blank" title="Click to Open">Chapter 6 - Application of Derivatives</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Integrals.pdf" target="_blank" title="Click to Open">Chapter 7 - Integrals</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Integrals.pdf" target="_blank" title="Click to Open">Chapter 8 - Application of Integrals</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Differential%20Equations.pdf" target="_blank" title="Click to Open">Chapter 9 - Differential Equations</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Vectors.pdf" target="_blank" title="Click to Open">Chapter 10 - Vector Algebra</a></li>
                              <li class="list-group-item"><a href="../data/subjective/Three%20Dimensional%20Geometry.pdf" target="_blank" title="Click to Open">Chapter 11 - Three Dimensional Geometry</a></li>
                              <li class="list-group-item">Chapter 12 - Linear Programming</li>
                              <li class="list-group-item">Chapter 13 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse3" id="symbolTest">Test Series &#9662;</a>
                           </h2>
                        </div>
                        <!--Test Series -->
                        <div id="collapse3" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item"><a href="../data/tests/Relations%20and%20Functions.pdf" target="_blank" title="Click to Open"> Chapter 1 - Relations and Functions </a></li>
                              <li class="list-group-item"><a href="../data/tests/Inverse%20Trigonometric%20Functions.pdf" target="_blank" title="Click to Open">Chapter 2 - Inverse Trigonometric Functions</a></li>
                              <li class="list-group-item"><a href="../data/tests/Matrices.pdf" target="_blank" title="Click to Open">Chapter 3 - Matrices</a></li>
                              <li class="list-group-item"><a href="../data/tests/Determinants.pdf" target="_blank" title="Click to Open">Chapter 4 - Determinants</a></li>
                              <li class="list-group-item"><a href="../data/tests/Continuity%20and%20Differentiability.pdf" target="_blank" title="Click to Open">Chapter 5 - Continuity and Differentiability</a></li>
                              <li class="list-group-item"><a href="../data/tests/Application%20of%20Derivatives.pdf" target="_blank" title="Click to Open">Chapter 6 - Application of Derivatives</a></li>
                              <li class="list-group-item"><a href="../data/tests/Integrals.pdf" target="_blank" title="Click to Open">Chapter 7 - Integrals</a></li>
                              <li class="list-group-item"><a href="../data/tests/Application%20of%20Integrals.pdf" target="_blank" title="Click to Open">Chapter 8 - Application of Integrals</a></li>
                              <li class="list-group-item"><a href="../data/tests/Differential%20Equations.pdf" target="_blank" title="Click to Open">Chapter 9 - Differential Equations</a></li>
                              <li class="list-group-item"><a href="../data/tests/Vector%20Algebra.pdf" target="_blank" title="Click to Open">Chapter 10 - Vector Algebra</a></li>
                              <li class="list-group-item"><a href="../data/tests/Three%20Dimensional%20Geometry.pdf" target="_blank" title="Click to Open">Chapter 11 - Three Dimensional Geometry</a></li>
                              <li class="list-group-item"><a href="../data/tests/Linear%20Programming.pdf" target="_blank" title="Click to Open">Chapter 12 - Linear Programming</a></li>
                              <li class="list-group-item"><a href="../data/tests/Probability.pdf" target="_blank" title="Click to Open">Chapter 13 - Probability</a></li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php
         }else{
         
         ?>
      <section class="page-section cta">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 mx-auto outer">
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse1" id="symbolObjective" title="Click to view content.">Objective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse1" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Relations and Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Inverse Trigonometric Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Matrices</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Determinants</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Continuity and Differentiability</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Application of Derivatives</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 7 - Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Application of Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Differential Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Vector Algebra</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Three Dimensional Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Linear Programming</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse2" id="symbolSubjective" title="Click to view content.">Subjective Type Questions &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse2" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Relations and Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Inverse Trigonometric Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Matrices</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Determinants</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Continuity and Differentiability</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Application of Derivatives</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 7 - Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Application of Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Differential Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Vector Algebra</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Three Dimensional Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Linear Programming</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
                  <div class="panel-group">
                     <div class="panel panel-default">
                        <div class="panel-heading">
                           <h2 class="panel-title" >
                              <a data-toggle="collapse" href="#collapse3" id="symbolTest" title="Click to view content.">Test Series &#9662;</a>
                           </h2>
                        </div>
                        <div id="collapse3" class="panel-collapse collapse">
                           <ul class="list-group">
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 1 - Relations and Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 2 - Inverse Trigonometric Functions</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 3 - Matrices</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 4 - Determinants</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 5 - Continuity and Differentiability</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 6 - Application of Derivatives</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 7 - Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 8 - Application of Integrals</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 9 - Differential Equations</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 10 - Vector Algebra</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 11 - Three Dimensional Geometry</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 12 - Linear Programming</li>
                              <li class="list-group-item" title="Preview Only! Signup to access content.">Chapter 13 - Probability</li>
                           </ul>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <?php
         }
         ?> 
      <footer class="footer text-faded text-center py-5">
         <div class="container">
            <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="../vendor/jquery/jquery.min.js"></script>
      <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
      <!-- customised js for this curriculum pages -->
      <script src="../js/curriculum.js"></script> 
			<?php 
			if( $login === 0 ){
		?>
		<script src="../js/alrt.js"></script> 
		<?php
			}
		?>
   </body>
</html>
