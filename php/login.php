

<?php
/*
	<!--

    Refernce for email regex: https://www.regular-expressions.info/email.html

	-->
*/

   session_start(); /* Starts the session */
   
	$valid = true;
   $err="";
	$try=0;
   $status;
   require_once("../Private/inputHelper.php");
   require_once("../Private/loginHelpers.php");
   
   if(isset($_SESSION['UserData']['Email'])){
   	$status=2;
		
   }
   if(isset($_POST['submit'])){
   	$form_email=$_POST['email'];
   	$form_psw=$_POST['psw'];


	if (!has_presence($form_psw) || !has_presence($form_email)) {
        //bad things happenned
        $valid = false;
    }
    if ( !has_length($form_psw, ['min' => 6, 'max' => 20]) ||  !has_length($form_email, ['min' => 6, 'max' => 40]) ) {
        //bad things happenned
        $valid = false;
    }

    $re = '/^(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){255,})(?!(?:(?:\x22?\x5C[\x00-\x7E]\x22?)|(?:\x22?[^\x5C\x22]\x22?)){65,}@)(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22))(?:\.(?:(?:[\x21\x23-\x27\x2A\x2B\x2D\x2F-\x39\x3D\x3F\x5E-\x7E]+)|(?:\x22(?:[\x01-\x08\x0B\x0C\x0E-\x1F\x21\x23-\x5B\x5D-\x7F]|(?:\x5C[\x00-\x7F]))*\x22)))*@(?:(?:(?!.*[^.]{64,})(?:(?:(?:xn--)?[a-z0-9]+(?:-[a-z0-9]+)*\.){1,126}){1,}(?:(?:[a-z][a-z0-9]*)|(?:(?:xn--)[a-z0-9]+))(?:-[a-z0-9]+)*)|(?:\[(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){7})|(?:(?!(?:.*[a-f0-9][:\]]){7,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,5})?)))|(?:(?:IPv6:(?:(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){5}:)|(?:(?!(?:.*[a-f0-9]:){5,})(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3})?::(?:[a-f0-9]{1,4}(?::[a-f0-9]{1,4}){0,3}:)?)))?(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))(?:\.(?:(?:25[0-5])|(?:2[0-4][0-9])|(?:1[0-9]{2})|(?:[1-9]?[0-9]))){3}))\]))$/';
   
    if (! has_format_matching($form_email, $re )){
        $valid = false;
			$err = $err."Email entered is not valid! \\n";

    }

		if($valid){
			$form_psw = htmlspecialchars($form_psw);
			$form_email = htmlspecialchars($form_email);
			if(attempt_login($form_email, $form_psw) ){
				$_SESSION['UserData']['Email']=$form_email;
				$status = 1;
			
			}else{
				$status = 0;
				$try=1;
			}

		}
   }

   
   
   if($status === 1 || $status === 2){
   	if(isset($_SESSION['UserData']['Email'])){
   		if($_SESSION['UserData']['Grade'] == '9'){
   			header("location: 9.php");
   		}
   		elseif($_SESSION['UserData']['Grade'] == '10'){
   			header("location: 10.php");
   		}
   		elseif($_SESSION['UserData']['Grade'] == '11'){
   			header("location: 11.php");
   		}
   		elseif($_SESSION['UserData']['Grade'] == '12'){
   			header("location: 12.php");
   		}
   		exit;
   	}

   }


?>
<!DOCTYPE html>
<html lang="en">
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <title>Login: RK's Maths Tutorial</title>
      <!-- Bootstrap core CSS -->
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom fonts for this template -->
      <!-- Custom styles for this template -->
      <link href="../css/business-casual.min.css" rel="stylesheet">
      <link rel="stylesheet" type="text/css" href="../css/login.css" >
   </head>
   <body id="main" >
      <h1 class="site-heading text-center text-white d-none d-lg-block">
         <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
         <!-- <span class="site-heading-lower">Business Casual</span> -->
      </h1>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
         <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">RK's Maths Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item px-lg-4">
                     <a class="nav-link text-uppercase text-expanded" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >About</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >Curriculum</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                        </li>
                     </ul>
                  </li>
               </ul>
               <ul class=" navbar-nav mx-auto">
                  <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>

                  <li id="c" class="nav-item px-lg-4 active"><a class="nav-link text-uppercase text-expanded" href="login.php" style="color: silver;" ><span class="glyphicon glyphicon-log-in"></span> Login</a></li>

               
               </ul>
            </div>
         </div>
      </nav>
      <form class="login-content" method="POST">
         <div class="container">
            <h1>Login</h1>
            <p>Please fill the fields to login to your account.</p>
            <label for="email"><b>Email*</b></label>
            <br><input type="email" placeholder="Enter Email" id="email" name="email" required>
            <br><label for="psw"><b>Password*</b></label>
            <input type="password" placeholder="Enter Password" id="psw" name="psw" required>
            <div class="clearfix">
               
               <button type="submit" class="loginbtn" name="submit">Login to my account</button>
            </div>
         </div>
      </form>
      <!--
         <?php
            if(!($status === 1 || $status === 2)){
            	echo "Don't have an account? Create one: ";
            	echo "<a href=\"signup.php\">Signup</a>";
            }
          ?>
         -->
      
      <footer class="footer text-faded text-center py-5">
         <div class="container">
            <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="../vendor/jquery/jquery.min.js"></script>
      <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
		<?php
		if(isset($_POST['submit'])){
			if(!($status === 1 || $status === 2)){
			
		?>
		
		<script src="../js/alrt3.js"></script> 
		<?php
			}
		}
		?>
		<script>
         var mod = document.getElementById('id01');
      	window.onclick = function(event) {
         if (event.target == mod) {
         	mod.style.display = "none";
             }
         }
      </script>

   </body>
</html>


