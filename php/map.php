

<?php
   session_start(); /* Starts the session */
   if(!isset($_SESSION['UserData']['Email'])){
   	$login = 0;//user not logged in
   }
   else{
   	$login = 1;//user logged in
   }
?>
<!DOCTYPE html>
<html>
   <head>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
      <meta name="description" content="">
      <meta name="author" content="">
      <title>FAQ's</title>
      <!-- Bootstrap core CSS -->
      <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
      <!-- Custom fonts for this template -->
      <!-- Custom styles for this template -->
      <link href="../css/business-casual.min.css" rel="stylesheet">
      <link href="../css/faq.css" rel="stylesheet">
   </head>
   <body>
      <h1 class="site-heading text-center text-white d-none d-lg-block">
         <span class="site-heading-upper text-primary mb-3"><a style="color:#e6a756" href="index.php" >RK's Maths Tutorial</a></span>
         <!-- <span class="site-heading-lower">Business Casual</span> -->
      </h1>
      <!-- Navigation -->
      <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
         <div class="container">
            <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">RK's Maths Tutorial</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarResponsive">
               <ul class="navbar-nav mx-auto">
                  <li class="nav-item px-lg-4">
                     <a class="nav-link text-uppercase text-expanded" style="color: silver;" href="index.php">Home
                     <span class="sr-only">(current)</span>
                     </a>
                  </li>
                  <li class="nav-item active px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >About</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4 active">
                           <a class="nav-link text-uppercase text-expanded" href="faq.php">FAQ</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="batches.php">Batches</a>
                        </li>
                     </ul>
                  </li>
                  <li class="nav-item px-lg-4 dropdown">
                     <a class="nav-link text-uppercase text-expanded dropdown-toggle" href="#" data-toggle="dropdown" >Curriculum</a>
                     <ul class="dropdown-menu">
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="9.php">9</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="10.php">10</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="11.php">11</a>
                        </li>
                        <li class="nav-item px-lg-4">
                           <a class="nav-link text-uppercase text-expanded" href="12.php">12</a>
                        </li>
                     </ul>
                  </li>
               </ul>
               <?php
                  if($login === 1){
                  ?>
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4">
                     <i  class="nav-link  text-expanded" >
                     <?php 
                        echo "Hello, ";					
                        echo $_SESSION['UserData']['Firstname'];
                        ?>
                     </i><span class="glyphicon glyphicon-log-in"></span>
                  </li>
                  <li class="nav-item px-lg-4"><a class="nav-link text-uppercase text-expanded" href=" logout.php"><span class="glyphicon glyphicon-log-in"></span> Logout</a></li>
               </ul>
               <?php
                  }elseif($login === 0){
               ?>
               </ul>
               <ul class="navbar-nav mx-auto ">
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="signup.php"><span class="glyphicon glyphicon-user"></span> Sign Up</a></li>
                  <li class="nav-item px-lg-4 "><a class="nav-link text-uppercase text-expanded" href="login.php"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
               </ul>
               <?php
                  }
               ?>
            </div>
         </div>
      </nav><br>
      <h2 style="color:white; text-align:center; "> Find us on Google Maps! </h2>
      <section class="page-section cta">
         <div class="container">
            <div class="row">
               <div class="col-xl-12 mx-auto">
                  <div class="cta-inner text-center rounded">
                     <div id="map" style="width:100%;height:500px"></div>
                     <script>
                        function myMap() {
                          var myCenter = new google.maps.LatLng(30.877275,75.833599);
                          var mapCanvas = document.getElementById("map");
                          var mapOptions = {center: myCenter, zoom: 5};
                          var map = new google.maps.Map(mapCanvas, mapOptions);
                          var marker = new google.maps.Marker({position:myCenter});
                          marker.setMap(map);
                        }
                     </script>
                     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBq7fMsE8WQkWKd677dBXVqRqHPvlbpF2k&callback=myMap"></script>
                  </div>
               </div>
            </div>
         </div>
      </section>
      <footer class="footer text-faded text-center py-5">
         <div class="container">
            <p class="m-0 small">Copyright &copy; The Explorers 2018</p>
         </div>
      </footer>
      <!-- Bootstrap core JavaScript -->
      <script src="../vendor/jquery/jquery.min.js"></script>
      <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
   </body>
</html>


